package ru.t1consulting.vmironova.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.dto.model.UserDTO;

@Repository
@Scope("prototype")
public interface UserDTORepository extends AbstractDTORepository<UserDTO> {

    @NotNull
    UserDTO findByLogin(@NotNull final String login);

    @NotNull
    UserDTO findByEmail(@NotNull final String email);

    @Query("select case when count(c)> 0 then true else false end from UserDTO c where login = :login")
    boolean isLoginExists(@Param("login") String login);

    @Query("select case when count(c)> 0 then true else false end from UserDTO c where email = :email")
    boolean isEmailExists(@Param("email") String email);

}
