package ru.t1consulting.vmironova.tm.api.service.model;

import org.jetbrains.annotations.Nullable;
import ru.t1consulting.vmironova.tm.model.Session;

public interface ISessionService extends IUserOwnedService<Session> {

    boolean existsById(@Nullable String userId, @Nullable String id) throws Exception;

}
