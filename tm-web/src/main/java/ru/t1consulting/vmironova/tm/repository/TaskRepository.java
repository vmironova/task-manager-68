package ru.t1consulting.vmironova.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Scope;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.t1consulting.vmironova.tm.model.Project;
import ru.t1consulting.vmironova.tm.model.Task;

import java.util.List;

@Repository
@Scope("prototype")
public interface TaskRepository extends JpaRepository<Task, String> {

    @NotNull
    List<Task> findByProject(@NotNull final Project project);

}
