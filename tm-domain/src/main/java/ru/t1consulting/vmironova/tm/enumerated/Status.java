package ru.t1consulting.vmironova.tm.enumerated;

import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Arrays;
import java.util.Optional;

@Getter
public enum Status {

    NOT_STARTED("Not started"),
    IN_PROGRESS("In progress"),
    COMPLETED("Completed");

    @NotNull
    private final String displayName;

    Status(@NotNull final String displayName) {
        this.displayName = displayName;
    }

    @NotNull
    public static String toName(@NotNull final Status status) {
        return status.getDisplayName();
    }

    @Nullable
    public static Status toStatus(@NotNull final String value) {
        if (value.isEmpty()) return null;
        @Nullable final Optional<Status> statusOptional = Arrays.stream(values()).filter(m -> value.equals(m.name())).findFirst();
        return statusOptional.orElse(null);
    }

}
